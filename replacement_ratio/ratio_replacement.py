# Note: Update the path and the file for each college , in this file as well as the avg_ratio_vietnam.py totally 6 locations
# Import the necessary modules
import pandas as pd
#-----set the directory for import
import os
import sys
file_dir = os.path.dirname("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Scripts/replacement_ratio/")
sys.path.append(file_dir)
#-----------------
from avg_ratio_HE_exc_RSCH import calc_avg_ratio_HE_exc_RSCH
from avg_ratio_VE_RSCH import calc_avg_ratio_VE_RSCH
from avg_ratio_offshore import calc_avg_ratio_offshore
from avg_ratio_vietnam import calc_avg_ratio_vietnam

# Load input, FoE and parameters data into python environ
#os.chdir("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/")
input_data = pd.read_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/InputData_Test/FinalInput/2021_May/Input_DSC_Jun2021.xlsx')
FoE_data = pd.read_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/InputData_Test/FinalInput/2021_May/Program_FOE_DSC.xlsx')
parameters_data = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Scripts/replacement_ratio/ratio_replacement_parameters.xlsx", header=None)
input_data['Broad_Fund'] = input_data['Broad_Fund'].replace({"INTOFFSH_exc_SIM": "INTOFFSH-exc-SIM", "INTOFFSH_SIM": "INTOFFSH-SIM", "GOVOPGR_FT": "GOVOPGR-FT","APPTRAIN_FT": "APPTRAIN-FT", "GOVOP_SH": "GOVOP-SH"}) #2nd: remove to match with data wrangling, # 1st:Amir added this line instead of in the subsections, Note: GOVO_SH cannot be added in subsections because does not exist in 2017-2019

# Call functions to get replacement ratios for all cohorts
replacement_ratio_HE_exc_RSCH = calc_avg_ratio_HE_exc_RSCH(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_VE_RSCH = calc_avg_ratio_VE_RSCH(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_offshore = calc_avg_ratio_offshore(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_vietnam = calc_avg_ratio_vietnam(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])

# Create replacement ratios for cohorts excluding vietnam
replacement_ratio_exc_vietnam = pd.concat([replacement_ratio_HE_exc_RSCH, replacement_ratio_VE_RSCH, replacement_ratio_offshore])

# Export replacement ratios as excel files
replacement_ratio_exc_vietnam.to_excel('./Avg_Ratio_Input/replacement_ratios_DSC_June2021.xlsx', index=False)
#replacement_ratio_vietnam.to_excel('./Avg_Ratio_Input/replacement_ratios_vietnam_DSC_Jun2021.xlsx', index=False)