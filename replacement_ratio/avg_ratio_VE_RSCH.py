# Import the necessary modules
import pandas as pd
import numpy as np
import os

os.chdir("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival")

def calc_avg_ratio_VE_RSCH(input_data, FoE_data, ratio_years, forecast_years):

    input_data['Study_Year'] = input_data['Study_Year'].replace({"C3": "C2", "C3_R": "C2_R"})
    #input_data['Broad_Fund'] = input_data['Broad_Fund'].replace({"INTOFFSH_exc_SIM": "INTOFFSH-exc-SIM", "INTOFFSH_SIM": "INTOFFSH-SIM", "GOVOPGR_FT": "GOVOPGR-FT"}) Amir removed from here and moved to the main mosule at start

    # Remove extraneous columns & map FoE to program code
    FoE_data = FoE_data[['Program_Code', 'Major_Disc']]
    merged_data = input_data.merge(FoE_data, how='left', on='Program_Code')

    # Filter data to keep rows corresponding to 2017-19, HE excluding research onshore
    merged_data_17_19 = merged_data[(merged_data['Year'] == ratio_years[1]) | (merged_data['Year'] == ratio_years[2]) | (merged_data['Year'] == ratio_years[3])]
    merged_data_20_25 = merged_data[(merged_data['Year'] == forecast_years[1]) | (merged_data['Year'] == forecast_years[2]) | (merged_data['Year'] == forecast_years[3]) | (merged_data['Year'] == forecast_years[4]) | (merged_data['Year'] == forecast_years[5]) | (merged_data['Year'] == forecast_years[6])]

    merged_data_17_19['Program_BroadFund_Combination'] = merged_data_17_19['School'] + '_' + merged_data_17_19['Career'] + '_' + merged_data_17_19['Program_Code'] + '_' + merged_data_17_19['Broad_Fund']
    merged_data_20_25['Program_BroadFund_Combination'] = merged_data_20_25['School'] + '_' + merged_data_20_25['Career'] + '_' + merged_data_20_25['Program_Code'] + '_' + merged_data_20_25['Broad_Fund']

    new_programs = merged_data_20_25[~merged_data_20_25['Program_BroadFund_Combination'].isin(merged_data_17_19['Program_BroadFund_Combination'])]

    merged_data = merged_data_17_19.drop(['Program_BroadFund_Combination'], 1)
    new_programs = new_programs.drop(['Program_BroadFund_Combination'], 1)

    merged_data = merged_data[(merged_data['Career'] != 'SUBD') & (merged_data['Career'] != 'UGRD') & (merged_data['Career'] != 'PGRD')]
    new_programs = new_programs[(new_programs['Career'] != 'SUBD') & (new_programs['Career'] != 'UGRD') & (new_programs['Career'] != 'PGRD')]
    merged_data = merged_data[(merged_data['Broad_Fund'] != 'FEEPAYOL') & (merged_data['Broad_Fund'] != 'INTOFFSH-exc-SIM') & (merged_data['Broad_Fund'] != 'INTOFFSH-SIM') & (merged_data['Broad_Fund'] != 'INTOFFSH') & (merged_data['Broad_Fund'] != 'INDONES')  & (merged_data['Broad_Fund'] != 'VIETNAM')]
    new_programs = new_programs[(new_programs['Broad_Fund'] != 'FEEPAYOL') & (new_programs['Broad_Fund'] != 'INTOFFSH-exc-SIM') & (new_programs['Broad_Fund'] != 'INTOFFSH-SIM') & (new_programs['Broad_Fund'] != 'INTOFFSH') & (new_programs['Broad_Fund'] != 'INDONES')  & (new_programs['Broad_Fund'] != 'VIETNAM')]

    # Define broader fund and 2-digit FoE column
    merged_data['Broader_Fund'] = np.where((merged_data['Broad_Fund'] == 'INTONSC') | (merged_data['Broad_Fund'] == 'INTONSH'), 'INTON', 'DOM')
    new_programs['Broader_Fund'] = np.where((new_programs['Broad_Fund'] == 'INTONSC') | (new_programs['Broad_Fund'] == 'INTONSH'), 'INTON', 'DOM')
    merged_data['Major_Disc'] = merged_data['Major_Disc'].replace(np.nan, 0)
    new_programs['Major_Disc'] = new_programs['Major_Disc'].replace(np.nan, 0)
    merged_data['Major_Disc (2-Digit)'] = merged_data['Major_Disc'].astype(str).str[:1].astype(int)
    new_programs['Major_Disc (2-Digit)'] = new_programs['Major_Disc'].astype(str).str[:1].astype(int)

    # Calculate aggregate study years based on career, broader fund & major discipline (4-digit)
    agg_data_df1 = merged_data.groupby(['Year', 'Career', 'Broader_Fund', 'Major_Disc', 'Study_Year'], as_index=False).agg({'EFTSL': 'sum'})
    agg_data_df2 = merged_data.groupby(['Year', 'Career', 'Broader_Fund', 'Major_Disc (2-Digit)', 'Study_Year'], as_index=False).agg({'EFTSL': 'sum'})
    agg_data_df3 = merged_data.groupby(['Year', 'Career', 'Broader_Fund', 'Study_Year'], as_index=False).agg({'EFTSL': 'sum'})

    agg_data_df1 = agg_data_df1.pivot_table(index=['Career','Broader_Fund', 'Major_Disc'],columns=['Year', 'Study_Year'],values='EFTSL',fill_value=0)
    agg_data_df1 = agg_data_df1.reset_index()
    agg_data_df2 = agg_data_df2.pivot_table(index=['Career','Broader_Fund', 'Major_Disc (2-Digit)'], columns=['Year', 'Study_Year'],values='EFTSL',fill_value=0)
    agg_data_df2 = agg_data_df2.reset_index()
    agg_data_df3 = agg_data_df3.pivot_table(index=['Career','Broader_Fund'],columns=['Year', 'Study_Year'], values='EFTSL',fill_value=0)
    agg_data_df3 = agg_data_df3.reset_index()

    # Calculate average replacement ratio for each study year
    agg_data_df1['C1R_rr1'] = agg_data_df1[ratio_years[2]]['C1_R']/agg_data_df1[ratio_years[1]]['C1']
    agg_data_df1['C2R_rr1'] = agg_data_df1[ratio_years[2]]['C2_R']/agg_data_df1[ratio_years[1]]['C2']
    agg_data_df1['YR3_rr1'] = agg_data_df1[ratio_years[2]]['YR3']/(agg_data_df1[ratio_years[1]]['C1_R'] + agg_data_df1[ratio_years[1]]['C2_R']) 
    agg_data_df1['YR4_rr1'] = agg_data_df1[ratio_years[2]]['YR4']/agg_data_df1[ratio_years[1]]['YR3']
    agg_data_df1['YR5_rr1'] = agg_data_df1[ratio_years[2]]['YR5+']/agg_data_df1[ratio_years[1]]['YR4']

    agg_data_df1['C1R_rr2'] = agg_data_df1[ratio_years[3]]['C1_R']/agg_data_df1[ratio_years[2]]['C1']
    agg_data_df1['C2R_rr2'] = agg_data_df1[ratio_years[3]]['C2_R']/agg_data_df1[ratio_years[2]]['C2']
    agg_data_df1['YR3_rr2'] = agg_data_df1[ratio_years[3]]['YR3']/(agg_data_df1[ratio_years[2]]['C1_R'] + agg_data_df1[ratio_years[2]]['C2_R']) 
    agg_data_df1['YR4_rr2'] = agg_data_df1[ratio_years[3]]['YR4']/agg_data_df1[ratio_years[2]]['YR3']
    agg_data_df1['YR5_rr2'] = agg_data_df1[ratio_years[3]]['YR5+']/agg_data_df1[ratio_years[2]]['YR4']

    agg_data_df1['C1R/C1'] = np.where(((agg_data_df1['C1R_rr1'] == 0) | (agg_data_df1['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df1['C1R_rr1']))) & ((agg_data_df1['C1R_rr2'] == 0) | (agg_data_df1['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df1['C1R_rr2']))), np.nan, np.where((agg_data_df1['C1R_rr1'] == 0) | (agg_data_df1['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df1['C1R_rr1'])),agg_data_df1['C1R_rr2'], np.where((agg_data_df1['C1R_rr2'] == 0) | (agg_data_df1['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df1['C1R_rr2'])), agg_data_df1['C1R_rr1'], ((agg_data_df1['C1R_rr1'] + agg_data_df1['C1R_rr2'])/2))))
    agg_data_df1['C2R/C2'] = np.where(((agg_data_df1['C2R_rr1'] == 0) | (agg_data_df1['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df1['C2R_rr1']))) & ((agg_data_df1['C2R_rr2'] == 0) | (agg_data_df1['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df1['C2R_rr2']))), np.nan, np.where((agg_data_df1['C2R_rr1'] == 0) | (agg_data_df1['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df1['C2R_rr1'])),agg_data_df1['C2R_rr2'], np.where((agg_data_df1['C2R_rr2'] == 0) | (agg_data_df1['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df1['C2R_rr2'])), agg_data_df1['C2R_rr1'], ((agg_data_df1['C2R_rr1'] + agg_data_df1['C2R_rr2'])/2))))
    agg_data_df1['YR3/YR2'] = np.where(((agg_data_df1['YR3_rr1'] == 0) | (agg_data_df1['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR3_rr1']))) & ((agg_data_df1['YR3_rr2'] == 0) | (agg_data_df1['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR3_rr2']))), np.nan, np.where((agg_data_df1['YR3_rr1'] == 0) | (agg_data_df1['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR3_rr1'])),agg_data_df1['YR3_rr2'], np.where((agg_data_df1['YR3_rr2'] == 0) | (agg_data_df1['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR3_rr2'])), agg_data_df1['YR3_rr1'], ((agg_data_df1['YR3_rr1'] + agg_data_df1['YR3_rr2'])/2))))
    agg_data_df1['YR4/YR3'] = np.where(((agg_data_df1['YR4_rr1'] == 0) | (agg_data_df1['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR4_rr1']))) & ((agg_data_df1['YR4_rr2'] == 0) | (agg_data_df1['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR4_rr2']))), np.nan, np.where((agg_data_df1['YR4_rr1'] == 0) | (agg_data_df1['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR4_rr1'])),agg_data_df1['YR4_rr2'], np.where((agg_data_df1['YR4_rr2'] == 0) | (agg_data_df1['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR4_rr2'])), agg_data_df1['YR4_rr1'], ((agg_data_df1['YR4_rr1'] + agg_data_df1['YR4_rr2'])/2))))
    agg_data_df1['YR5/YR4'] = np.where(((agg_data_df1['YR5_rr1'] == 0) | (agg_data_df1['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR5_rr1']))) & ((agg_data_df1['YR5_rr2'] == 0) | (agg_data_df1['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR5_rr2']))), np.nan, np.where((agg_data_df1['YR5_rr1'] == 0) | (agg_data_df1['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df1['YR5_rr1'])),agg_data_df1['YR5_rr2'], np.where((agg_data_df1['YR5_rr2'] == 0) | (agg_data_df1['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df1['YR5_rr2'])), agg_data_df1['YR5_rr1'], ((agg_data_df1['YR5_rr1'] + agg_data_df1['YR5_rr2'])/2))))

    agg_data_df2['C1R_rr1'] = agg_data_df2[ratio_years[2]]['C1_R']/agg_data_df2[ratio_years[1]]['C1']
    agg_data_df2['C2R_rr1'] = agg_data_df2[ratio_years[2]]['C2_R']/agg_data_df2[ratio_years[1]]['C2']
    agg_data_df2['YR3_rr1'] = agg_data_df2[ratio_years[2]]['YR3']/(agg_data_df2[ratio_years[1]]['C1_R'] + agg_data_df2[ratio_years[1]]['C2_R']) 
    agg_data_df2['YR4_rr1'] = agg_data_df2[ratio_years[2]]['YR4']/agg_data_df2[ratio_years[1]]['YR3']
    agg_data_df2['YR5_rr1'] = agg_data_df2[ratio_years[2]]['YR5+']/agg_data_df2[ratio_years[1]]['YR4']

    agg_data_df2['C1R_rr2'] = agg_data_df2[ratio_years[3]]['C1_R']/agg_data_df2[ratio_years[2]]['C1']
    agg_data_df2['C2R_rr2'] = agg_data_df2[ratio_years[3]]['C2_R']/agg_data_df2[ratio_years[2]]['C2']
    agg_data_df2['YR3_rr2'] = agg_data_df2[ratio_years[3]]['YR3']/(agg_data_df2[ratio_years[2]]['C1_R'] + agg_data_df2[ratio_years[2]]['C2_R']) 
    agg_data_df2['YR4_rr2'] = agg_data_df2[ratio_years[3]]['YR4']/agg_data_df2[ratio_years[2]]['YR3']
    agg_data_df2['YR5_rr2'] = agg_data_df2[ratio_years[3]]['YR5+']/agg_data_df2[ratio_years[2]]['YR4']

    agg_data_df2['C1R/C1'] = np.where(((agg_data_df2['C1R_rr1'] == 0) | (agg_data_df2['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df2['C1R_rr1']))) & ((agg_data_df2['C1R_rr2'] == 0) | (agg_data_df2['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df2['C1R_rr2']))), np.nan, np.where((agg_data_df2['C1R_rr1'] == 0) | (agg_data_df2['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df2['C1R_rr1'])),agg_data_df2['C1R_rr2'], np.where((agg_data_df2['C1R_rr2'] == 0) | (agg_data_df2['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df2['C1R_rr2'])), agg_data_df2['C1R_rr1'], ((agg_data_df2['C1R_rr1'] + agg_data_df2['C1R_rr2'])/2))))
    agg_data_df2['C2R/C2'] = np.where(((agg_data_df2['C2R_rr1'] == 0) | (agg_data_df2['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df2['C2R_rr1']))) & ((agg_data_df2['C2R_rr2'] == 0) | (agg_data_df2['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df2['C2R_rr2']))), np.nan, np.where((agg_data_df2['C2R_rr1'] == 0) | (agg_data_df2['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df2['C2R_rr1'])),agg_data_df2['C2R_rr2'], np.where((agg_data_df2['C2R_rr2'] == 0) | (agg_data_df2['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df2['C2R_rr2'])), agg_data_df2['C2R_rr1'], ((agg_data_df2['C2R_rr1'] + agg_data_df2['C2R_rr2'])/2))))
    agg_data_df2['YR3/YR2'] = np.where(((agg_data_df2['YR3_rr1'] == 0) | (agg_data_df2['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR3_rr1']))) & ((agg_data_df2['YR3_rr2'] == 0) | (agg_data_df2['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR3_rr2']))), np.nan, np.where((agg_data_df2['YR3_rr1'] == 0) | (agg_data_df2['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR3_rr1'])),agg_data_df2['YR3_rr2'], np.where((agg_data_df2['YR3_rr2'] == 0) | (agg_data_df2['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR3_rr2'])), agg_data_df2['YR3_rr1'], ((agg_data_df2['YR3_rr1'] + agg_data_df2['YR3_rr2'])/2))))
    agg_data_df2['YR4/YR3'] = np.where(((agg_data_df2['YR4_rr1'] == 0) | (agg_data_df2['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR4_rr1']))) & ((agg_data_df2['YR4_rr2'] == 0) | (agg_data_df2['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR4_rr2']))), np.nan, np.where((agg_data_df2['YR4_rr1'] == 0) | (agg_data_df2['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR4_rr1'])),agg_data_df2['YR4_rr2'], np.where((agg_data_df2['YR4_rr2'] == 0) | (agg_data_df2['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR4_rr2'])), agg_data_df2['YR4_rr1'], ((agg_data_df2['YR4_rr1'] + agg_data_df2['YR4_rr2'])/2))))
    agg_data_df2['YR5/YR4'] = np.where(((agg_data_df2['YR5_rr1'] == 0) | (agg_data_df2['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR5_rr1']))) & ((agg_data_df2['YR5_rr2'] == 0) | (agg_data_df2['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR5_rr2']))), np.nan, np.where((agg_data_df2['YR5_rr1'] == 0) | (agg_data_df2['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df2['YR5_rr1'])),agg_data_df2['YR5_rr2'], np.where((agg_data_df2['YR5_rr2'] == 0) | (agg_data_df2['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df2['YR5_rr2'])), agg_data_df2['YR5_rr1'], ((agg_data_df2['YR5_rr1'] + agg_data_df2['YR5_rr2'])/2))))

    agg_data_df3['C1R_rr1'] = agg_data_df3[ratio_years[2]]['C1_R']/agg_data_df3[ratio_years[1]]['C1']
    agg_data_df3['C2R_rr1'] = agg_data_df3[ratio_years[2]]['C2_R']/agg_data_df3[ratio_years[1]]['C2']
    agg_data_df3['YR3_rr1'] = agg_data_df3[ratio_years[2]]['YR3']/(agg_data_df3[ratio_years[1]]['C1_R'] + agg_data_df3[ratio_years[1]]['C2_R']) 
    agg_data_df3['YR4_rr1'] = agg_data_df3[ratio_years[2]]['YR4']/agg_data_df3[ratio_years[1]]['YR3']
    agg_data_df3['YR5_rr1'] = agg_data_df3[ratio_years[2]]['YR5+']/agg_data_df3[ratio_years[1]]['YR4']

    agg_data_df3['C1R_rr2'] = agg_data_df3[ratio_years[3]]['C1_R']/agg_data_df3[ratio_years[2]]['C1']
    agg_data_df3['C2R_rr2'] = agg_data_df3[ratio_years[3]]['C2_R']/agg_data_df3[ratio_years[2]]['C2']
    agg_data_df3['YR3_rr2'] = agg_data_df3[ratio_years[3]]['YR3']/(agg_data_df3[ratio_years[2]]['C1_R'] + agg_data_df3[ratio_years[2]]['C2_R']) 
    agg_data_df3['YR4_rr2'] = agg_data_df3[ratio_years[3]]['YR4']/agg_data_df3[ratio_years[2]]['YR3']
    agg_data_df3['YR5_rr2'] = agg_data_df3[ratio_years[3]]['YR5+']/agg_data_df3[ratio_years[2]]['YR4']

    agg_data_df3['C1R/C1'] = np.where(((agg_data_df3['C1R_rr1'] == 0) | (agg_data_df3['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df3['C1R_rr1']))) & ((agg_data_df3['C1R_rr2'] == 0) | (agg_data_df3['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df3['C1R_rr2']))), np.nan, np.where((agg_data_df3['C1R_rr1'] == 0) | (agg_data_df3['C1R_rr1'].isna()) | (~np.isfinite(agg_data_df3['C1R_rr1'])),agg_data_df3['C1R_rr2'], np.where((agg_data_df3['C1R_rr2'] == 0) | (agg_data_df3['C1R_rr2'].isna()) | (~np.isfinite(agg_data_df3['C1R_rr2'])), agg_data_df3['C1R_rr1'], ((agg_data_df3['C1R_rr1'] + agg_data_df3['C1R_rr2'])/2))))
    agg_data_df3['C2R/C2'] = np.where(((agg_data_df3['C2R_rr1'] == 0) | (agg_data_df3['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df3['C2R_rr1']))) & ((agg_data_df3['C2R_rr2'] == 0) | (agg_data_df3['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df3['C2R_rr2']))), np.nan, np.where((agg_data_df3['C2R_rr1'] == 0) | (agg_data_df3['C2R_rr1'].isna()) | (~np.isfinite(agg_data_df3['C2R_rr1'])),agg_data_df3['C2R_rr2'], np.where((agg_data_df3['C2R_rr2'] == 0) | (agg_data_df3['C2R_rr2'].isna()) | (~np.isfinite(agg_data_df3['C2R_rr2'])), agg_data_df3['C2R_rr1'], ((agg_data_df3['C2R_rr1'] + agg_data_df3['C2R_rr2'])/2))))
    agg_data_df3['YR3/YR2'] = np.where(((agg_data_df3['YR3_rr1'] == 0) | (agg_data_df3['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR3_rr1']))) & ((agg_data_df3['YR3_rr2'] == 0) | (agg_data_df3['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR3_rr2']))), np.nan, np.where((agg_data_df3['YR3_rr1'] == 0) | (agg_data_df3['YR3_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR3_rr1'])),agg_data_df3['YR3_rr2'], np.where((agg_data_df3['YR3_rr2'] == 0) | (agg_data_df3['YR3_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR3_rr2'])), agg_data_df3['YR3_rr1'], ((agg_data_df3['YR3_rr1'] + agg_data_df3['YR3_rr2'])/2))))
    agg_data_df3['YR4/YR3'] = np.where(((agg_data_df3['YR4_rr1'] == 0) | (agg_data_df3['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR4_rr1']))) & ((agg_data_df3['YR4_rr2'] == 0) | (agg_data_df3['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR4_rr2']))), np.nan, np.where((agg_data_df3['YR4_rr1'] == 0) | (agg_data_df3['YR4_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR4_rr1'])),agg_data_df3['YR4_rr2'], np.where((agg_data_df3['YR4_rr2'] == 0) | (agg_data_df3['YR4_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR4_rr2'])), agg_data_df3['YR4_rr1'], ((agg_data_df3['YR4_rr1'] + agg_data_df3['YR4_rr2'])/2))))
    agg_data_df3['YR5/YR4'] = np.where(((agg_data_df3['YR5_rr1'] == 0) | (agg_data_df3['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR5_rr1']))) & ((agg_data_df3['YR5_rr2'] == 0) | (agg_data_df3['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR5_rr2']))), np.nan, np.where((agg_data_df3['YR5_rr1'] == 0) | (agg_data_df3['YR5_rr1'].isna()) | (~np.isfinite(agg_data_df3['YR5_rr1'])),agg_data_df3['YR5_rr2'], np.where((agg_data_df3['YR5_rr2'] == 0) | (agg_data_df3['YR5_rr2'].isna()) | (~np.isfinite(agg_data_df3['YR5_rr2'])), agg_data_df3['YR5_rr1'], ((agg_data_df3['YR5_rr1'] + agg_data_df3['YR5_rr2'])/2))))

    # Drop extraneous columns and replace inf/0 with missing values
    agg_data_df1 = agg_data_df1[['Career', 'Broader_Fund', 'Major_Disc', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    agg_data_df1.columns = agg_data_df1.columns.get_level_values(0)

    agg_data_df2 = agg_data_df2[['Career', 'Broader_Fund', 'Major_Disc (2-Digit)', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    agg_data_df2.columns = agg_data_df1.columns.get_level_values(0)
    agg_data_df2 = agg_data_df2.rename(columns={'Major_Disc': 'Major_Disc (2-Digit)'})

    agg_data_df3 = agg_data_df3[['Career', 'Broader_Fund', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    agg_data_df3.columns = agg_data_df3.columns.get_level_values(0)

    # Map average replacement ratios to program broadfunds
    mapped_data_1a = pd.merge(merged_data, agg_data_df1, on=['Career', 'Broader_Fund', 'Major_Disc'], how='left')
    mapped_data_1b = pd.merge(new_programs, agg_data_df1, on=['Career', 'Broader_Fund', 'Major_Disc'], how='left')
    mapped_data1 = pd.concat([mapped_data_1a, mapped_data_1b])
    mapped_data1 = mapped_data1[['School', 'Career', 'Broad_Fund', 'Program_Code', 'Major_Disc', 'Broader_Fund', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    mapped_data1 = mapped_data1.drop_duplicates()

    mapped_data_2a = pd.merge(merged_data, agg_data_df2, on=['Career', 'Broader_Fund', 'Major_Disc (2-Digit)'], how='left')
    mapped_data_2b = pd.merge(new_programs, agg_data_df2, on=['Career', 'Broader_Fund', 'Major_Disc (2-Digit)'], how='left')
    mapped_data2 = pd.concat([mapped_data_2a, mapped_data_2b])
    mapped_data2 = mapped_data2[['School', 'Career', 'Broad_Fund', 'Program_Code', 'Major_Disc (2-Digit)', 'Broader_Fund', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    mapped_data2 = mapped_data2.drop_duplicates()

    mapped_data_3a = pd.merge(merged_data, agg_data_df3, on=['Career', 'Broader_Fund'], how='left')
    mapped_data_3b = pd.merge(new_programs, agg_data_df3, on=['Career', 'Broader_Fund'], how='left')
    mapped_data3 = pd.concat([mapped_data_3a, mapped_data_3b])
    mapped_data3 = mapped_data3[['School', 'Career', 'Broad_Fund', 'Program_Code', 'Broader_Fund', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    mapped_data3 = mapped_data3.drop_duplicates()

    # Create new columns for program-broadfund and career-broaderfund-FoE combinations
    mapped_data1['Program_BroadFund_Combination'] = mapped_data1['School'] + '_' + mapped_data1['Career'] + '_' + mapped_data1['Program_Code'] + '_' + mapped_data1['Broad_Fund']
    mapped_data1['Career_BroaderFund_FoE'] = mapped_data1['Career'] + '-' + mapped_data1['Broader_Fund'] + '-' + mapped_data1['Major_Disc'].astype(int).astype(str)
    mapped_data1 = mapped_data1.drop(['School','Career','Program_Code','Broad_Fund','Broader_Fund','Major_Disc'], 1)

    mapped_data2['Program_BroadFund_Combination'] = mapped_data2['School'] + '_' + mapped_data2['Career'] + '_' + mapped_data2['Program_Code'] + '_' + mapped_data2['Broad_Fund']
    mapped_data2['Career_BroaderFund_FoE'] = mapped_data2['Career'] + '-' + mapped_data2['Broader_Fund'] + '-' + mapped_data2['Major_Disc (2-Digit)'].astype(int).astype(str)
    mapped_data2 = mapped_data2.drop(['School','Career','Program_Code','Broad_Fund','Broader_Fund','Major_Disc (2-Digit)'], 1)

    mapped_data3['Program_BroadFund_Combination'] = mapped_data3['School'] + '_' + mapped_data3['Career'] + '_' + mapped_data3['Program_Code'] + '_' + mapped_data3['Broad_Fund']
    mapped_data3['Career_BroaderFund_FoE'] = mapped_data3['Career'] + '-' + mapped_data3['Broader_Fund']
    mapped_data3 = mapped_data3.drop(['School','Career','Program_Code','Broad_Fund','Broader_Fund'], 1)

    # Reorder columns
    mapped_data1 = mapped_data1[['Program_BroadFund_Combination', 'Career_BroaderFund_FoE', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    mapped_data2 = mapped_data2[['Program_BroadFund_Combination', 'Career_BroaderFund_FoE', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]
    mapped_data3 = mapped_data3[['Program_BroadFund_Combination', 'Career_BroaderFund_FoE', 'C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]

    cols_to_check = ['C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']

    mapped_data1['is_na'] = mapped_data1[cols_to_check].isnull().apply(lambda x: all(x), axis=1) 

    mapped_data1 = mapped_data1.reset_index(drop=True)
    mapped_data2 = mapped_data2.reset_index(drop=True)
    mapped_data3 = mapped_data3.reset_index(drop=True)

    mapped_data1_no_ratios1 = mapped_data1[mapped_data1['is_na']==True]
    if(len(mapped_data1_no_ratios1) > 0):
        mapped_data1.loc[mapped_data1['Program_BroadFund_Combination'].isin(mapped_data1_no_ratios1['Program_BroadFund_Combination']), ['C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']] = mapped_data2[['C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]

    mapped_data1['is_na_2'] = mapped_data1[cols_to_check].isnull().apply(lambda x: all(x), axis=1) 

    mapped_data1_no_ratios2 = mapped_data1[mapped_data1['is_na_2']==True]
    if(len(mapped_data1_no_ratios2) > 0):
        mapped_data1.loc[mapped_data1['Program_BroadFund_Combination'].isin(mapped_data1_no_ratios2['Program_BroadFund_Combination']), ['C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']] = mapped_data3[['C1R/C1', 'C2R/C2', 'YR3/YR2', 'YR4/YR3', 'YR5/YR4']]

    mapped_data = mapped_data1.drop(['is_na', 'is_na_2'], 1)
    mapped_data = mapped_data[mapped_data['Program_BroadFund_Combination'].isin(merged_data_20_25['Program_BroadFund_Combination'])]

    return(mapped_data)

# # Export as excel file
# mapped_data.to_excel('./Avg_Ratio_Input/avg_ratio_VE_RSCH.xlsx', index=False)
