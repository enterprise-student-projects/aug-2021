# in 10 locations the name of college should be updated in paths and files....
# Import the necessary modules
import pandas as pd
#-----set the directory for import
import os
import sys
file_dir = os.path.dirname("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Scripts/rpt_group_location/")
sys.path.append(file_dir)
#-----------------
from Group_Location_HE_exc_RSCH import rpt_HE_exc_RSCH
from Group_Location_VE import rpt_VE
from Group_Location_RSCH import rpt_RSCH
from Group_Location_offshore import rpt_offshore
from Group_Location_vietnam import rpt_vietnam
from datetime import datetime


# Load the forecast output and RPT actuals into Python environment
forecast_output_HE_exc_RSCH = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Intermediate_Outputs/SEH_May2021/forecast_output_HE_exc_RSCH_2021May.xlsx")
forecast_output_VE = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Intermediate_Outputs/SEH_May2021/forecast_output_VE_2021May.xlsx")
forecast_output_RSCH = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Intermediate_Outputs/SEH_May2021/forecast_output_RSCH_2021May.xlsx")
forecast_output_offshore = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Intermediate_Outputs/SEH_May2021/forecast_output_offshore_2021May.xlsx")
forecast_output_vietnam = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Intermediate_Outputs/SEH_May2021/forecast_output_vietnam_2021May.xlsx")

# Load intermediate input into Python environment
parameters_data = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/Scripts/Rpt_Group_Location/rpt_group_location_params_SEH.xlsx", header=None)
year = str(parameters_data.iloc[1][1])

# Load RPT location group actuals into Python Environment 
rpt_actuals = pd.read_excel("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/InputData_Test/RPT_Location_Group/RPT_Location_Group_Actuals.xlsx", sheet_name=year)

# Replace INTOFFSH with the appropriate broad fund based on Principal RPT Location
rpt_actuals.loc[rpt_actuals['Principal_RPT_Location'] == 'SIM', 'BROAD_FUND_2'] = 'INTOFFSH-SIM'
rpt_actuals['BROAD_FUND_2'] = rpt_actuals['BROAD_FUND_2'].replace({"INTOFFSH": "INTOFFSH-exc-SIM"})

# Call function to split forecast output for the various cohorts into RPT group and location
output_HE_exc_RSCH, output_HE_exc_RSCH_rpt = rpt_HE_exc_RSCH(parameters_data.iloc[0][1], forecast_output_HE_exc_RSCH, rpt_actuals)
output_VE, output_VE_rpt = rpt_VE(parameters_data.iloc[0][1], forecast_output_VE, rpt_actuals)
output_RSCH, output_RSCH_rpt = rpt_RSCH(parameters_data.iloc[0][1], forecast_output_RSCH, rpt_actuals)
output_offshore, output_offshore_rpt = rpt_offshore(parameters_data.iloc[0][1], forecast_output_offshore, rpt_actuals)
output_vietnam, output_vietnam_rpt = rpt_vietnam(parameters_data.iloc[0][1], forecast_output_vietnam, rpt_actuals)

# Combine the forecast outputs for all onshore cohorts and offshore cohorts separately
output_RSCH_onshore =  output_RSCH[(output_RSCH['Broad_Fund'] != 'INTOFFSH-exc-SIM') & (output_RSCH['Broad_Fund'] != 'INTOFFSH-SIM') & (output_RSCH['Broad_Fund'] != 'INTOFFSH')]
output_RSCH_onshore_rpt = output_RSCH_rpt[(output_RSCH_rpt['Broad_Fund'] != 'INTOFFSH-exc-SIM') & (output_RSCH_rpt['Broad_Fund'] != 'INTOFFSH-SIM') & (output_RSCH_rpt['Broad_Fund'] != 'INTOFFSH')]

output_RSCH_offshore = output_RSCH[(output_RSCH['Broad_Fund'] == 'INTOFFSH-exc-SIM') | (output_RSCH['Broad_Fund'] == 'INTOFFSH-SIM') | (output_RSCH['Broad_Fund'] == 'INTOFFSH')]
output_RSCH_offshore_rpt = output_RSCH_rpt[(output_RSCH_rpt['Broad_Fund'] == 'INTOFFSH-exc-SIM') | (output_RSCH_rpt['Broad_Fund'] == 'INTOFFSH-SIM') | (output_RSCH_rpt['Broad_Fund'] == 'INTOFFSH')]

output_RSCH_offshore = output_RSCH_offshore.drop(columns=['Equivalent_SCH'])
output_RSCH_offshore_rpt = output_RSCH_offshore_rpt.drop(columns=['Equivalent_SCH'])

final_output_onshore = pd.concat([output_HE_exc_RSCH, output_VE, output_RSCH_onshore])
final_output_offshore = pd.concat([output_RSCH_offshore, output_offshore, output_vietnam])

final_output_onshore_rpt = pd.concat([output_HE_exc_RSCH_rpt, output_VE_rpt, output_RSCH_onshore_rpt])
final_output_offshore_rpt = pd.concat([output_RSCH_offshore_rpt, output_offshore_rpt, output_vietnam_rpt])

# Export the final onshore and offshore forecast outputs as excel files
final_output_onshore.to_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/ForecastsOutput/SEH_May2021/final_output_onshore_' + datetime.today().strftime('%Y_%m_%d') + '.xlsx', index=False)
final_output_offshore.to_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/ForecastsOutput/SEH_May2021/final_output_offshore_' + datetime.today().strftime('%Y_%m_%d') + '.xlsx', index=False)

final_output_onshore_rpt.to_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/ForecastsOutput/SEH_May2021/final_output_onshore_RPT_' + datetime.today().strftime('%Y_%m_%d') + '.xlsx', index=False)
final_output_offshore_rpt.to_excel('K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/ForecastsOutput/SEH_May2021/final_output_offshore_RPT_' + datetime.today().strftime('%Y_%m_%d') + '.xlsx', index=False)