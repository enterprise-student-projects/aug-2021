# Import the necessary modules
import pandas as pd
from avg_ratio_HE_exc_RSCH import calc_avg_ratio_HE_exc_RSCH
from avg_ratio_VE_RSCH import calc_avg_ratio_VE_RSCH
from avg_ratio_offshore import calc_avg_ratio_offshore
from avg_ratio_vietnam import calc_avg_ratio_vietnam

# Load input, FoE and parameters data into python environ
input_data = pd.read_excel('./InputData_Test/FinalInput/Input_BUS_V8.xlsx')
FoE_data = pd.read_excel('./InputData_Test/Program_FOE_BUS.xlsx')
parameters_data = pd.read_excel("./Scripts/replacement_ratio/ratio_replacement_parameters.xlsx", header=None)

# Call functions to get replacement ratios for all cohorts
replacement_ratio_HE_exc_RSCH = calc_avg_ratio_HE_exc_RSCH(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_VE_RSCH = calc_avg_ratio_VE_RSCH(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_offshore = calc_avg_ratio_offshore(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])
replacement_ratio_vietnam = calc_avg_ratio_vietnam(input_data, FoE_data, parameters_data.iloc[0], parameters_data.iloc[1])

# Create replacement ratios for cohorts excluding vietnam
replacement_ratio_exc_vietnam = pd.concat([replacement_ratio_HE_exc_RSCH, replacement_ratio_VE_RSCH, replacement_ratio_offshore])

# Export replacement ratios as excel files
replacement_ratio_exc_vietnam.to_excel('./Avg_Ratio_Input/replacement_ratios_test.xlsx', index=False)
replacement_ratio_vietnam.to_excel('./Avg_Ratio_Input/replacement_ratios_vietnam_test.xlsx', index=False)