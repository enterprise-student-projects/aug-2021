# Import the necessary modules
import pandas as pd
import numpy as np
import os

os.chdir("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival/")

def rpt_RSCH(college, forecast_output, rpt_actuals):
 
        # Filter RPT actuals to include the relevant scope
        rpt_actuals = rpt_actuals[(rpt_actuals['PORTFOLIO_CODE'] == college)]
        rpt_actuals = rpt_actuals[(rpt_actuals['Career'] == 'RSCH')]
        rpt_actuals = rpt_actuals[(rpt_actuals['BROAD_FUND_2'] != 'FEEPAYOL') & (rpt_actuals['BROAD_FUND_2'] != 'INDONES')  & (rpt_actuals['BROAD_FUND_2'] != 'VIETNAM')]

        # Split program broadfunds into group, location and calculate split eftsl ratio
        agg_df_1 = rpt_actuals.groupby(['Career', 'SCHOOL_CODE', 'BROAD_FUND_2', 'Program_Code'], as_index=False).agg({'EFTSL': 'sum'})
        agg_df_2 = rpt_actuals.groupby(['Career', 'SCHOOL_CODE', 'BROAD_FUND_2', 'Program_Code', 'Principal_RPT_Group', 'Principal_RPT_Location'], as_index=False).agg({'EFTSL': 'sum'})
        agg_df = agg_df_2.merge(agg_df_1, how='left', on=['Career', 'SCHOOL_CODE', 'BROAD_FUND_2', 'Program_Code'])
        agg_df['proportion'] = agg_df['EFTSL_x']/agg_df['EFTSL_y']

        # Melt forecast output
        forecast_output = forecast_output.rename(columns={'Unnamed: 0': 'Year'})
        forecast_output = forecast_output.melt(id_vars=["Year"], 
                var_name="Program_BroadFund_Combination", 
                value_name="Forecasted_EFTSL")

        # Split program broadfund column into separate columns
        forecast_output[['School', 'Career', 'Program_Code', 'Broad_Fund']] = forecast_output["Program_BroadFund_Combination"].str.split("_", expand=True)
        forecast_output = forecast_output.drop(columns=['Program_BroadFund_Combination'])

        # Merge forecast output with group, locations and eftsl split ratios
        final_df = forecast_output.merge(agg_df, how='left', left_on=['Program_Code', 'Broad_Fund'], right_on=['Program_Code', 'BROAD_FUND_2'])
        final_df['proportion'] = final_df['proportion'].replace(np.nan, 1)

        # Multiply forecasted eftsl with group, location eftsl split
        final_df['Forecasted_EFTSL'] = final_df['Forecasted_EFTSL'] * final_df['proportion']

        # Drop extraneous columns
        final_df = final_df.drop(columns=['Career_y', 'SCHOOL_CODE', 'BROAD_FUND_2', 'EFTSL_x', 'EFTSL_y', 'proportion'])
        final_df = final_df.rename(columns={'Career_x' : 'Career'})

        # Sort final RPT forecast output according to career, program code, broadfund, Principal RPT Group and Principal RPT Location
        final_df = final_df.sort_values(['Career', 'Program_Code', 'Broad_Fund', 'Principal_RPT_Group', 'Principal_RPT_Location'], ascending=[True, True, True, True, True]) 

        # Create RPT Status and Equivelent SCH columns in final RPT forecast output
        final_df['RPT_Status'] = ['Returning' for x in range(len(final_df))]
        final_df['Equivalent_SCH'] = ""
        
        # Reorder columns in final RPT forecast output
        final_df = final_df[['Year', 'School', 'Career', 'Program_Code', 'Broad_Fund', 'RPT_Status', 'Forecasted_EFTSL', 'Equivalent_SCH', 'Principal_RPT_Group', 'Principal_RPT_Location']]

        # Sort final forecast output according to career, program code, broadfund
        forecast_output = forecast_output.sort_values(['Career', 'Program_Code', 'Broad_Fund'], ascending=[True, True, True]) 

        # Create RPT Status and Equivelent SCH columns in final forecast output
        forecast_output['RPT_Status'] = ['Returning' for x in range(len(forecast_output))]
        forecast_output['Equivalent_SCH'] = ""
        
        # Reorder columns in final forecast output
        forecast_output = forecast_output[['Year', 'School', 'Career', 'Program_Code', 'Broad_Fund', 'RPT_Status', 'Forecasted_EFTSL', 'Equivalent_SCH']]

        return(forecast_output, final_df)