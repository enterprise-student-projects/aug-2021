setwd("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival")

avg_ratio_data <- read_excel('Avg_Ratio_Input/replacement_ratios_SEH_June2021.xlsx')

check_avg_ratio <- function(rr_2018, rr_2019) {
  avg_return_rate = 0
  if(is.na(rr_2018) || rr_2018 == 0 || is.infinite(rr_2018)) {
    if(is.na(rr_2019) || rr_2019 == 0 || is.infinite(rr_2019)) {
      avg_return_rate = 0
    } else {
     avg_return_rate = rr_2019
    }
  } else {
    if(is.na(rr_2019) || rr_2019 == 0 || is.infinite(rr_2019)) {
      avg_return_rate = rr_2018
    } else {
      avg_return_rate = (rr_2018 + rr_2019)/2
    }
  }

  return(avg_return_rate)
}


calculate_dist<-function(data, h6_R, datapoints) {
  
C1R<-vector(mode="numeric")
C2R<-vector(mode="numeric")
Y3R<-vector(mode="numeric")
Y4R<-vector(mode="numeric")
Y5R<-vector(mode="numeric")

program_fund_colnames <-  stri_replace_last_fixed(colnames(h6_R), '_R', '')
print(length(data))
for (j in 1:length(data)) {
  
  if(program_fund_colnames[j] %in% avg_ratio_data$Program_BroadFund_Combination) {

  C1R_rr_2018 = data[[j]][[datapoints-2,2]]/data[[j]][[datapoints-3,1]]
  C1R_rr_2019 = data[[j]][[datapoints-1,2]]/data[[j]][[datapoints-2,1]]
  
  agg_return_rate_C1R = check_avg_ratio(C1R_rr_2018, C1R_rr_2019)
  
  C2R_rr_2018 = data[[j]][[datapoints-2,4]]/data[[j]][[datapoints-3,3]]
  C2R_rr_2019 = data[[j]][[datapoints-1,4]]/data[[j]][[datapoints-2,3]]
  
  agg_return_rate_C2R = check_avg_ratio(C2R_rr_2018, C2R_rr_2019)
  
  Y3R_rr_2018 = data[[j]][[datapoints-2,5]]/data[[j]][[datapoints-3,8]]
  Y3R_rr_2019 = data[[j]][[datapoints-1,5]]/data[[j]][[datapoints-2,8]]
  
  agg_return_rate_Y3R = check_avg_ratio(Y3R_rr_2018, Y3R_rr_2019)
  
  Y4R_rr_2018 = data[[j]][[datapoints-2,6]]/data[[j]][[datapoints-3,5]]
  Y4R_rr_2019 = data[[j]][[datapoints-1,6]]/data[[j]][[datapoints-2,5]]
  
  agg_return_rate_Y4R = check_avg_ratio(Y4R_rr_2018, Y4R_rr_2019)
  
  Y5R_rr_2018 = data[[j]][[datapoints-2,7]]/data[[j]][[datapoints-3,6]]
  Y5R_rr_2019 = data[[j]][[datapoints-1,7]]/data[[j]][[datapoints-2,6]]
  
  agg_return_rate_Y5R = check_avg_ratio(Y5R_rr_2018, Y5R_rr_2019) 
  
    C1R[j] = agg_return_rate_C1R
    C2R[j] = agg_return_rate_C2R
    Y3R[j] = agg_return_rate_Y3R
    Y4R[j] = agg_return_rate_Y4R
    Y5R[j] = agg_return_rate_Y5R
    }
  }

ratio_distribution_prog <- data.frame(program_fund_colnames, C1R, C2R, Y3R, Y4R, Y5R)
ratio_distribution_prog <- ratio_distribution_prog[(ratio_distribution_prog$program_fund_colnames %in% avg_ratio_data$Program_BroadFund_Combination),]

return(ratio_distribution_prog)

}

h6_R_list = list(h6_R_HE_exc_RSCH, h6_R_VE, h6_R_RSCH, h6_R_offshore)
data_list = list(h6.2L_HE_exc_RSCH, h6.2L_VE, h6.2L_RSCH, h6.2L_offshore)

ratio_dist_HE_exc_RSCH = data.frame()
ratio_dist_VE = data.frame()
ratio_dist_RSCH = data.frame()
ratio_dist_offshore = data.frame()

for(i in 1:length(data_list)) {
  ratio_distribution <- calculate_dist(data_list[[i]], h6_R_list[[i]], 4)
  
  if(i==1) {
    ratio_dist_HE_exc_RSCH = ratio_distribution
  } else if(i==2) {
    ratio_dist_VE = ratio_distribution
  } else if(i==3) {
    ratio_dist_RSCH  = ratio_distribution
  } else if(i==4) {
    ratio_dist_offshore = ratio_distribution
  }
}
#ratio_distribution <- calculate_dist(data_list[[4]], h6_R_list[[4]], 4)

write.xlsx(ratio_dist_HE_exc_RSCH, "RatioDistribution/ratio_distribution.xlsx", row.names = FALSE, sheetName = "ratio_dist_HE_exc_RSCH")
write.xlsx(ratio_dist_VE, "RatioDistribution/ratio_distribution.xlsx", row.names = FALSE, sheetName = "ratio_dist_VE", append=TRUE)
write.xlsx(ratio_dist_RSCH, "RatioDistribution/ratio_distribution.xlsx", row.names = FALSE, sheetName = "ratio_dist_RSCH", append=TRUE)
write.xlsx(ratio_dist_offshore, "RatioDistribution/ratio_distribution.xlsx", row.names = FALSE, sheetName = "ratio_dist_offshore", append=TRUE)
