
# Set working directory
setwd("K:/Off Gov and Plan/PPG/SRU/Analytics/39-ReturningLoad-Cohort_Survival")

avg_ratio_data <- read_excel('Avg_Ratio_Input/replacement_ratios_vietnam_SEH_May2021.xlsx')
cutoffs_data <- read_excel('Scripts/Forecasting/outlier_cutoffs_vietnam_SEH_May2021.xlsx', col_names = FALSE)

# Calculate final ratio based on whether return rates are missing or not
check_avg_ratio <- function(rr_2018, rr_2019, study_year, program_fundsource, cutoff) {
  avg_return_rate = 0
  if(is.na(rr_2018) || rr_2018 == 0 || is.infinite(rr_2018)) {
    if(is.na(rr_2019) || rr_2019 == 0 || is.infinite(rr_2019)) {
      avg_return_rate = avg_ratio_data[avg_ratio_data$Program_BroadFund_Combination==program_fundsource,][[study_year+2]]
    } else {
      if(rr_2019 > cutoff) {
        avg_return_rate = avg_ratio_data[avg_ratio_data$Program_BroadFund_Combination==program_fundsource,][[study_year+2]]
      } else {
        avg_return_rate = rr_2019
      }
    }
  } else {
    if(is.na(rr_2019) || rr_2019 == 0 || is.infinite(rr_2019)) {
      avg_return_rate = avg_ratio_data[avg_ratio_data$Program_BroadFund_Combination==program_fundsource,][[study_year+2]]
    } else {
      if(((rr_2018+rr_2019)/2) > cutoff) {
        avg_return_rate = avg_ratio_data[avg_ratio_data$Program_BroadFund_Combination==program_fundsource,][[study_year+2]]
      } else {
        avg_return_rate = (rr_2018 + rr_2019)/2
      } 
    }
  }
  
  return(avg_return_rate)
}

# Forecast cohort survival ratio model - 2YR avg -  for 2021-2025
calculate_2y_avg<-function(data, datapoints, no_years, cutoff_row) {
  
  Y2_avg<-matrix(NA,5,length(data)) 
  C1R<-vector(mode="numeric", length = 5)
  C2R<-vector(mode="numeric", length = 5)
  C3R<-vector(mode="numeric", length = 5)
  Y3R<-vector(mode="numeric", length = 5)
  Y4R<-vector(mode="numeric", length = 5)
  Y5R<-vector(mode="numeric", length = 5)
  
  program_fund_colnames <-  stri_replace_last_fixed(colnames(h6_R_VIETNAM), '_R', '')

  for (j in 1:length(data)) {
    
    if(program_fund_colnames[j] %in% avg_ratio_data$Program_BroadFund_Combination) {
    
    C1R_rr_2018 = data[[j]][[datapoints-2,2]]/data[[j]][[datapoints-3,1]]
    C1R_rr_2019 = data[[j]][[datapoints-1,2]]/data[[j]][[datapoints-2,1]]
    
    agg_return_rate_C1R = check_avg_ratio(C1R_rr_2018, C1R_rr_2019, 1, program_fund_colnames[j], as.numeric(cutoff_row[2]))
    
    C2R_rr_2018 = data[[j]][[datapoints-2,4]]/data[[j]][[datapoints-3,3]]
    C2R_rr_2019 = data[[j]][[datapoints-1,4]]/data[[j]][[datapoints-2,3]]
    
    agg_return_rate_C2R = check_avg_ratio(C2R_rr_2018, C2R_rr_2019, 2, program_fund_colnames[j], as.numeric(cutoff_row[3]))
    
    C3R_rr_2018 = data[[j]][[datapoints-2,6]]/data[[j]][[datapoints-3,5]]
    C3R_rr_2019 = data[[j]][[datapoints-1,6]]/data[[j]][[datapoints-2,5]]
    
    agg_return_rate_C3R = check_avg_ratio(C3R_rr_2018, C3R_rr_2019, 3, program_fund_colnames[j], as.numeric(cutoff_row[4]))
    
    Y3R_rr_2018 = data[[j]][[datapoints-2,7]]/data[[j]][[datapoints-3,10]]
    Y3R_rr_2019 = data[[j]][[datapoints-1,7]]/data[[j]][[datapoints-2,10]]
    
    agg_return_rate_Y3R = check_avg_ratio(Y3R_rr_2018, Y3R_rr_2019, 4, program_fund_colnames[j], as.numeric(cutoff_row[5]))
    
    Y4R_rr_2018 = data[[j]][[datapoints-2,8]]/data[[j]][[datapoints-3,7]]
    Y4R_rr_2019 = data[[j]][[datapoints-1,8]]/data[[j]][[datapoints-2,7]]
    
    agg_return_rate_Y4R = check_avg_ratio(Y4R_rr_2018, Y4R_rr_2019, 5, program_fund_colnames[j], as.numeric(cutoff_row[6]))
    
    Y5R_rr_2018 = data[[j]][[datapoints-2,9]]/data[[j]][[datapoints-3,8]]
    Y5R_rr_2019 = data[[j]][[datapoints-1,9]]/data[[j]][[datapoints-2,8]]

    agg_return_rate_Y5R = check_avg_ratio(Y5R_rr_2018, Y5R_rr_2019, 6, program_fund_colnames[j], as.numeric(cutoff_row[7])) 

    for(i in 1:no_years) {

        if(i==1) {
          
      C1R[i] <- agg_return_rate_C1R*(data[[j]][[datapoints,1]])
      C2R[i] <- agg_return_rate_C2R*(data[[j]][[datapoints,3]])
      C3R[i] <- agg_return_rate_C3R*(data[[j]][[datapoints,5]])
      Y3R[i] <- agg_return_rate_Y3R*(data[[j]][[datapoints,10]])
      Y4R[i] <- agg_return_rate_Y4R*(data[[j]][[datapoints,7]])
      Y5R[i] <- agg_return_rate_Y5R*(data[[j]][[datapoints,8]])

      } else {

        C1R[i] <- agg_return_rate_C1R*(data[[j]][[datapoints+(i-1),1]])
        C2R[i] <- agg_return_rate_C2R*(data[[j]][[datapoints+(i-1),3]])
        C3R[i] <- agg_return_rate_C3R*(data[[j]][[datapoints+(i-1),5]])
        Y3R[i] <- agg_return_rate_Y3R*(sum(C1R[i-1],C2R[i-1],C3R[i-1],na.rm=TRUE))
        Y4R[i] <- agg_return_rate_Y4R*(Y3R[i-1])
        Y5R[i] <- agg_return_rate_Y5R*(Y4R[i-1])
      }

      Y2_avg[[i,j]] <-sum(C1R[i],C2R[i],C3R[i],Y3R[i],Y4R[i],Y5R[i],na.rm=TRUE)
    }
  }
}

  return(Y2_avg)
  
}

# Produce forecasts for Vietnam
forecast_VIETNAM<-calculate_2y_avg(h6.2L_VIETNAM, 4, 5, cutoffs_data[1,])
program_fund_colnames <- stri_replace_last_fixed(colnames(h6_R_VIETNAM), '_R', '')
colnames(forecast_VIETNAM)<-program_fund_colnames
forecast_VIETNAM<-forecast_VIETNAM[, colSums(is.na(forecast_VIETNAM)) != nrow(forecast_VIETNAM)]
forecast_VIETNAM<- as.data.frame(forecast_VIETNAM)
row.names(forecast_VIETNAM) <- c("2022", "2023","2024", "2025", "2026")

write.xlsx(forecast_VIETNAM, "Intermediate_Outputs/forecast_output_vietnam_2021May.xlsx", row.names = TRUE)
